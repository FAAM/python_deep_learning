# Bienvenidos al repositorio python_deep_learning!
Conceptos básicos sobre Deep Learning con Tensorflow y Keras[D

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_deep_learning), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_deep_learning
```

## Contenidos

```{tableofcontents}
```